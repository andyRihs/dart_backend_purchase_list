import 'package:aqueduct/aqueduct.dart';
import 'package:dart_backend_purchase_list/model/purchase_list.dart';

import '../dart_backend_purchase_list.dart';
import '../model/product.dart';

class ProductController extends ResourceController {
  ProductController(this.context);

  final ManagedContext context;

  @Operation.get("id")
  Future<Response> getAllProducts(@Bind.path("id") int id) async {
    final productsQuery = Query<Product>(context)
      ..where((h) => h.purchaseList.id).equalTo(id);
    final products = await productsQuery.fetch();

    products.sort((a, b) => a.category.index.compareTo(b.category.index));

    return Response.ok(products);
  }

  //?? how to output possible category's in SwaggerUI ??
  @Operation.post("id")
  Future<Response> addProduct(
      @Bind.path("id")
          int id,
      {@Bind.body(ignore: ["productId", "creationDate", "id", "purchaseList"])
          Product newProduct}) async {

    //check if purchaseList.id exist
    final productListQuery = Query<PurchaseList>(context)
      ..where((h) => h.id).equalTo(id);
    final productList = await productListQuery.fetchOne();

    if (productList == null) {
      return Response.badRequest(
          body: {"error": "purchase list id not existing"});
    }

    newProduct.purchaseList = productList;
    newProduct.purchaseList.id = id;
    newProduct.creationDate = DateTime.now();

    final queryCreateProduct = Query<Product>(context)..values = newProduct;
    final insertedProduct = await queryCreateProduct.insert();

    return Response.ok(insertedProduct);
  }

  @Operation.put("id", "id2")
  Future<Response> updateProductById(
      @Bind.path("id")
          int id,
      @Bind.path("id2")
          int id2,
      @Bind.body(ignore: ["productId", "creationDate", "purchaseList", "id"])
          Product editedProduct) async {

    final updateProductQuery = Query<Product>(context)
      ..values = editedProduct
      ..where((h) => h.productId).equalTo(id2);

    final Product product = await updateProductQuery.updateOne();

    if (product != null) {
      return Response.ok(product);
    }

    return Response.badRequest();
  }

  @Operation.delete("id", "id2")
  Future<Response> deleteProductById(
      @Bind.path("id") int id, @Bind.path("id2") int id2) async {
    final deleteQuery = Query<Product>(context)
      ..where((h) => h.purchaseList.id).equalTo(id)
      ..where((h) => h.productId).equalTo(id2);

    final deletedHero = await deleteQuery.delete();

    if (deletedHero == null) {
      return Response.notFound();
    }

    return Response.ok(deletedHero);
  }

  //https://pub.dev/documentation/aqueduct/latest/aqueduct/ManagedObjectController/documentOperationRequestBody.html
  @override
  APIRequestBody documentOperationRequestBody(
      APIDocumentContext context, Operation operation) {
    if (operation.method == "POST" || operation.method == "PUT") {
      // return APIRequestBody.schema(context.schema.getObjectWithType(Product), //use the original object
          return APIRequestBody.schema(
          APISchemaObject.object({
            "productName": APISchemaObject.string(),
            "amount" : APISchemaObject.integer(),
            "unit" : APISchemaObject.string(),
            "category" : APISchemaObject.string(),
          }),
          required: true,
          description:
              "enter product details. \n amount and unit are optional. \n Possible categories are: bakery, fruts_vegetable, usw. ");
    }

    return null;
  }
}
