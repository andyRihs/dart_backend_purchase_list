import '../dart_backend_purchase_list.dart';
import '../model/purchase_list.dart';

class GroceryListController extends ResourceController {
  GroceryListController(this.context);

  final ManagedContext context;

  @Operation.get()
  Future<Response> getAllGroceryLists() async {
    final productListQuery = Query<PurchaseList>(context);
    final productList = await productListQuery.fetch();

    return Response.ok(productList);
  }

  @Operation.get("id")
  Future<Response> getGroceryListsById(@Bind.path("id") int id) async {
    final productListQuery = Query<PurchaseList>(context)
      ..where((h) => h.id).equalTo(id)
      ..join(set: (a) => a.products);

    final productList = await productListQuery.fetchOne();

    if (productList == null) {
      return Response.notFound();
    }

    productList.products
        .sort((a, b) => a.category.index.compareTo(b.category.index));
    return Response.ok(productList);
  }

  @Operation.post()
  Future<Response> createGroceryList(
      {@Bind.body(ignore: ["id", "dateOfCreation"], require: ["listName"])
          PurchaseList purchaseList}) async {
    if (purchaseList.listName == null) {
      return Response.noContent();
    }

    purchaseList.dateOfCreation = DateTime.now();
    final createListQuery = Query<PurchaseList>(context)..values = purchaseList;

    final insertNewList = await createListQuery.insert();

    return Response.ok(insertNewList);
  }

  @Operation.delete("id")
  Future<Response> deletePurchaseListById(@Bind.path("id") int id) async {
    final deleteQuery = Query<PurchaseList>(context)
      ..where((h) => h.id).equalTo(id);

    final deletePurchaseList = await deleteQuery.delete();
    if (deletePurchaseList == 0) {
      return Response.notFound();
    }

    return Response.ok(deletePurchaseList);
  }

  //https://pub.dev/documentation/aqueduct/latest/aqueduct/ManagedObjectController/documentOperationRequestBody.html
  @override
  APIRequestBody documentOperationRequestBody(
      APIDocumentContext context, Operation operation) {
    if (operation.method == "POST") {
      return APIRequestBody.schema(
          APISchemaObject.object({"listName": APISchemaObject.string()}),
          required: true,
          description: "to create a new purchase a listName is required");
    }

    return null;
  }

  //https://aqueduct.io/docs/openapi/endpoint/
  @override
  Map<String, APIResponse> documentOperationResponses(
      APIDocumentContext context, Operation operation) {
    if (operation.method == "GET") {
      if (operation.pathVariables.contains("id")) {
        return {
          "200": APIResponse.schema(
              "Return an list by its id", context.schema.getObjectWithType(PurchaseList)),
          "404": APIResponse("There is no object with this id"),
        };
      } else {
        return {
          "200": APIResponse("Return all lists"),
        };
      }
    } else if (operation.method == "POST") {
      return {
        "204": APIResponse("listName should not be null"),
        "400": APIResponse("listName is missing"),
        "200": APIResponse("list is created successfully"),
      };
    }

    return {
      "200": APIResponse("Successful response."),
    };
  }
}
