import '../dart_backend_purchase_list.dart';
import 'product.dart';

class PurchaseList extends ManagedObject<_PurchaseList>
    implements _PurchaseList, APIComponentDocumenter{}

class _PurchaseList {
  @primaryKey
  int id;

  @Column(unique: true, nullable: false)
  String listName;

  @Column(nullable: false)
  DateTime dateOfCreation;

  ManagedSet<Product> products;

}