import '../dart_backend_purchase_list.dart';
import 'category.dart';
import 'purchase_list.dart';

class Product extends ManagedObject<_Product> implements _Product {}

class _Product {
  @primaryKey
  int productId;

  String productName;

  @Column(nullable: true)
  double amount;

  @Column(nullable: true)
  String unit;

  DateTime creationDate;

  //needed?
  @Relate(#products, onDelete: DeleteRule.cascade)
  PurchaseList purchaseList;

  //emums are persisted as Strings
  Category category;
}
