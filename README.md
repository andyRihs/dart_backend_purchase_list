# dart_backend_pruchase_list

## Running the Application Locally

Run `aqueduct serve` from this directory to run the application. For running within an IDE, run `bin/main.dart`. By default, a configuration file named `config.yaml` will be used.
To generate a SwaggerUI client, run `aqueduct document client`.

Before run aqueduct you must start PostgreSQL within Docker:  
`docker run --name postgres -e POSTGRES_PASSWORD=mysecretpassword -v pgdata:/var/lib/postgresql/data -p 5432:5432 -d postgres`  

Afterwards, create Database using psql as follow:  
`CREATE DATABASE purchaselist;`  
`CREATE USER list_user WITH createdb;`  
`ALTER USER list_user WITH password 'password';`  
`GRANT all ON database purchaselist TO list_user;`  

As soon as the DB is running, you need also to initialise the DB tables using the migrations file (see folder migrations)  
`aqueduct db upgrade --connect postgres://list_user:password@localhost:5432/purchaselist`  

## Running Application Tests

There are no applications tests. 

## Deploying an Application

See the documentation for [Deployment](https://aqueduct.io/docs/deploy/)

## Update the DB after editing the "model"
1. `aqueduct db generate`
2. `aqueduct db upgrade --connect postgres://list_user:password@localhost:5432/purchaselist`

